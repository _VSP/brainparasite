﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TryKillingALimb : MonoBehaviour {

    // Use this for initialization

    public AudioSource HitSound;
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

     
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {

        var limbscript = collision.gameObject.GetComponent<RemoveLimb>();
        limbscript.BreakLimb();
        if(!HitSound.isPlaying)
            HitSound.Play();
          

    }
}
