﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heilautus : MonoBehaviour {

    // Use this for initialization
    HingeJoint2D Joint;
    float Then;
    float MotorOnTime = 0.5f;
    int MotorSwitch = -1;
    public ParticleSystem system;
    void Start () {
		Joint = GetComponent<HingeJoint2D>();

    }

    // Update is called once per frame
    void Update () {

        if (Time.timeSinceLevelLoad - Then > MotorOnTime)
        {
            if (gameObject.name != "Torso" && gameObject.name != "Torso2")
            {
                Joint.useMotor = false;
            }
        }

    }

    public void UseJoint()
    {
        Then = Time.timeSinceLevelLoad;

        if (gameObject.name == "Torso")
        {
            GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0, 20), (ForceMode2D.Impulse));

        }

        else
        {
            var motor = Joint.motor;

            if (motor.motorSpeed < 0)
                motor.motorSpeed = MotorSwitch * Joint.motor.motorSpeed;

            Joint.motor = motor;

            Joint.useMotor = true;

        }
    }

    public void UseJoint2()
    {
        Then = Time.timeSinceLevelLoad;

        if (gameObject.name == "Torso")
        {
            GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0, 20), (ForceMode2D.Impulse));
            
        }
        else if(gameObject.name == "Torso2")
        {

        }
            

        else
        {

            var motor = Joint.motor;

            if (motor.motorSpeed > 0)
                motor.motorSpeed = MotorSwitch * Joint.motor.motorSpeed;

            Joint.motor = motor;

            Joint.useMotor = true;

        }
    }


}
