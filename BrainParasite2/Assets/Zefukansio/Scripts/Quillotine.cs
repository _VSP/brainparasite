﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quillotine : MonoBehaviour {

    Rigidbody2D rigidBody;

    private void Awake()
    {
        rigidBody = gameObject.GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        StartCoroutine("wait3Seconds");
    }

    IEnumerator wait3Seconds()
    {
        yield return new WaitForSeconds(3f);
        reverseGravity();
    }

    public void reverseGravity()
    {
        if(rigidBody.gravityScale == -1)
            rigidBody.gravityScale = 1;
        else if (rigidBody.gravityScale == 1)
            rigidBody.gravityScale = -1;

        StartCoroutine("wait3Seconds");
    }




}
