﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCharacter : MonoBehaviour {

    GameObject Torso;
	// Use this for initialization
	void Start () {
        Torso = GameObject.Find("Torso");
	}
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.position = Torso.transform.position;
	}
}
