﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BrainButtonScript : MonoBehaviour {
    
    [SerializeField] string bodyPart;
    [SerializeField] AudioClip squish1;
    [SerializeField] AudioClip squish2;
    [SerializeField] AudioClip squish3;
    [SerializeField] AudioClip squish4;
    AudioClip[] auido;
    AudioSource audioSource;
    bool Shut = false;
    [SerializeField]Color brainColor;

    public void ShutDown()
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.black;
        Shut = true;
    }


    private void Awake()
    {
        if (Shut)
            return;
        auido = new AudioClip[] { squish1, squish2, squish3, squish4 };
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    IEnumerator waitHalfSecond()
    {

        yield return new WaitForSeconds(0.5f);
        gameObject.GetComponent<SpriteRenderer>().color = brainColor;
    }

    public void DoOnClick()
    {

        if (Shut)
            return;
        audioSource.clip = auido[UnityEngine.Random.Range(0, 4)];
        audioSource.Play();
        gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        StartCoroutine("waitHalfSecond");
        GameObject bodyPartGameObject;
        bodyPartGameObject = GameObject.Find(bodyPart);
        //aja kehonosasta haluttu funktio seuraavalla rivillä.
        bodyPartGameObject.GetComponent<Heilautus>().UseJoint();
    }

    public void DoOnClick2()
    {

        if (Shut)
            return;
        audioSource.clip = auido[UnityEngine.Random.Range(0, 4)];
        audioSource.Play();
        gameObject.GetComponent<SpriteRenderer>().color = Color.green;
        StartCoroutine("waitHalfSecond");
        GameObject bodyPartGameObject;
        bodyPartGameObject = GameObject.Find(bodyPart);
        //aja kehonosasta haluttu funktio seuraavalla rivillä.
        bodyPartGameObject.GetComponent<Heilautus>().UseJoint2();
    }
}
