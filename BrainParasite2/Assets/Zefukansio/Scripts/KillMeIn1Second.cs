﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillMeIn1Second : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine("waitSecond");
	}



    IEnumerator waitSecond()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
