﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }


    public GameObject particle;

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                hit.transform.gameObject.GetComponent<BrainButtonScript>().DoOnClick();
                if(hit.transform.gameObject.GetComponent<FartSoundScript>() != null)
                {
                    hit.transform.gameObject.GetComponent<FartSoundScript>().Fart();
                }
                }
        }
        else if(Input.GetButtonDown("Fire2"))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                hit.transform.gameObject.GetComponent<BrainButtonScript>().DoOnClick2();
            }
        }
    }
}
