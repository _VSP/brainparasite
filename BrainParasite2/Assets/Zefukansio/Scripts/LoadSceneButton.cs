﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadSceneButton : MonoBehaviour {


    [SerializeField] string SceneToLoad;
    // Use this for initialization

    private void Awake()
    {
        Button btn = gameObject.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {

        if (SceneToLoad == "Quit")
            Application.Quit();
        else
            SceneManager.LoadScene(SceneToLoad);

      


    }

    // Update is called once per frame
    void Update () {
		
	}
}
