﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trambouline : MonoBehaviour {

	// Use this for initialization
    private void OnCollisionEnter2D(Collision2D collision)
    {
      gameObject.transform.position = gameObject.transform.position + Vector3.down * 3;
        StartCoroutine("waitHalfSecond");
      collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 10000);   
    }


    IEnumerator waitHalfSecond()
    {
        yield return new WaitForSeconds(0.3f);
        gameObject.transform.position = gameObject.transform.position + Vector3.up * 3;
    }


}
