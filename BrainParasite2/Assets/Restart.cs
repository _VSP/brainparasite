﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Restart : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown("space"))
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        if (Input.GetKeyDown("escape"))
        {
            if (SceneManager.GetActiveScene().name != "Level Selection")
                SceneManager.LoadScene("Level Selection");
            else
                Application.Quit();
        }

    }
}
