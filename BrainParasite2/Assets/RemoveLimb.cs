﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RemoveLimb : MonoBehaviour {

    HingeJoint2D Joint;
    ParticleSystem Blood;
    public BrainButtonScript BrainPart;

    // Use this for initialization
    void Start () {
        if(name != "Torso" || name != "Torso2")
            Joint = GetComponent<HingeJoint2D>();

         Blood = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
    
    }

    public void BreakLimb()
    {
        if (BrainPart != null)
        {
            Debug.Log("SADSAD");
            BrainPart.ShutDown();
        }
        
        if (name == "Head")
            SceneManager.LoadScene("GameOver");

        if (name == "Torso" || name == "Torso2")
        {
            Blood.Emit(60);

            if(name == "Torso2")
            {
                GetComponent<SpringJoint2D>().enabled = false;
                GetComponent<RelativeJoint2D>().enabled = false;

            }
        }
        else
        {

            if (Joint.isActiveAndEnabled)
            {
                Joint.enabled = false;
                Blood.Emit(30);
            }
        }
    }

}
